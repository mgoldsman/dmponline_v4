# README #

Instrucciones para la instalacion.

### 1- Crear schema de DB###

* Usando MYSQL v5.0 o mayor
* Crear schema [dmp]
	CREATE DATABASE dmp;
* Crear usuario / pwd [dmp/dmp]
	GRANT ALL PRIVILEGES ON dmp.* to 'dmp'@'%' IDENTIFIED BY 'dmp';SHO

### Ref DMPonline_v4/config/database.yml ###

*   adapter: mysql2
*   database: dmp
*   hostname: 127.0.0.1
*   username: dmp
*   password: dmp
*   socket: /tmp/mysql.sock
*   pool: 5
*   timeout: 5000
*   encoding: utf8

### 2- Configurar docker ###
* sudo apt-get update
* sudo apt-get install docker-engine
* sudo service docker start
* sudo docker run hello-world 

# 3- Descargar Image ruby
* sudo docker pull mgoldsman/rubydmp

# 4- Crear working directory
* mkdir /home/dmp
* cd /home/dmp
* git clone https://bitbucket.org/mgoldsman/dmponline_v4.git
* cd dmponline_v4/

# 5- crear container docker
 * sudo docker run -i -t -d -p 52023:22 -p 3000:3000 --name rubydmp  -v /home/dmp/dmponline_v4:/usr/src/app  mgoldsman/rubydmp:latest

# 6- Conectar al container docker
* sudo  docker exec -it rubydmp bash

# 7- Configurar app (sobre container docker)
* cd /usr/src/app/
* bundle install
* rake db:setup
* rake db:migrate
	* rake db:schema:load
* rails server


# Comandos Anexos

### Configurar proyecto###

* bundle update


### Comandos para schema de DB###

* Usando MYSQL
* Crear schema
* Crear usuario / pwd
* verificar /config/database.yml
* ejecutar comando "rake db:setup"
* ejecutar comando "rake db:migrate"

### Levantar app ###

* rails server &

# Versiones
* ruby 2.2.0p0
* Rails 3.2.22

### DB by file ###

 * conectar al contenedor mysql
 	**cd /mysql
	**./mysql
 	**use dmp;
 	**source /mysql/Dump20161118.sql; commit;



###